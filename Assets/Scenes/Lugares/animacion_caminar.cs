using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animacion_caminar : MonoBehaviour
{
	private Animation anim;
	public float timer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
    	if (Input.GetKey(KeyCode.W))
    	{
    		caminando();
    	}
    	if (Input.GetKey(KeyCode.S))
    	{
    		caminando();
    	}
        else if(Input.GetKeyUp(KeyCode.W))
        {
            timer = 0;
        }
        else if(Input.GetKeyUp(KeyCode.S))
        {
            timer = 0;
        }
    }

    void caminando()
    {
    	timer += Time.deltaTime;
    	if(timer > 1)
    	{
    		anim.Play("paso_derecho");
    	}
    	if(timer < 1)
    	{
    		anim.Play("paso_izquierdo");	
    	}
    	if(timer >= 2)
    	{
    		timer = 0.0f;
    	}
    }
}
