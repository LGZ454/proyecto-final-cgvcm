using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class codigo_interactuable : MonoBehaviour
{
	public Color color_default;
	public Color color_over;
	private MeshRenderer render;
	public string titulo;
    public string mi_informacion;
    public GameObject luz;

    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<MeshRenderer>();
    }

    void OnMouseOver()
    {
        luz.SetActive(true);
    }

    void OnMouseExit()
    {
        luz.SetActive(false);
    }
}
