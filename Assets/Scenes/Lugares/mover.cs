using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class mover : MonoBehaviour
{
	public float despl = 40.0f;
	public float rotate = 15.0f;
	private float rotationX = 0.0f;
	private float rotationY = 0.0f;
	private float minY = -45.0f;
	private float maxY = 70.0f;
	public float sensitivity = 100.0f;

    public GameObject sonido_caminar;

	private bool pressKey;
	private int counter;

    private GameObject mi_caminar;

	//public GameObject sonido_caminar;
	//public GameObject mi_caminar;

    // Start is called before the first frame update
    void Start()
    {
        pressKey = false;
        counter = 0;
    }

    // Update is called once per frame
    void Update()
    {
    	pressKey = false;
	        if (Input.GetKey(KeyCode.W))
	        {
	        	transform.position += new Vector3(Time.deltaTime * despl * (float)Math.Sin(transform.rotation.eulerAngles.y * Math.PI / 180),
	        		0.0f, Time.deltaTime * despl * (float)Math.Cos(transform.rotation.eulerAngles.y * Math.PI / 180) );
	        	pressKey = true;
	        	counter = counter+1;
	        	//GetComponent<Camera> ().fieldOfView--;
	        }
	        else if (Input.GetKey(KeyCode.S))
	        {
	        	transform.position += new Vector3(-1 * Time.deltaTime * despl * (float)Math.Sin(transform.rotation.eulerAngles.y * Math.PI / 180),
	        		0.0f, -1 * Time.deltaTime * despl * (float)Math.Cos(transform.rotation.eulerAngles.y * Math.PI / 180) );
	        	pressKey = true;
	        	counter = counter+1;
	        	//GetComponent<Camera> ().fieldOfView++;
	        }
	        else if (Input.GetKey(KeyCode.A))
	        {
	        	transform.position += new Vector3(Time.deltaTime * despl * (float)Math.Sin( (transform.rotation.eulerAngles.y-90) * Math.PI / 180),
	        		0.0f, Time.deltaTime * despl * (float)Math.Cos( (transform.rotation.eulerAngles.y-90) * Math.PI / 180) );
	        	pressKey = true;
	        	counter = counter+1;
	        	//GetComponent<Camera> ().fieldOfView--;
	        }
	        else if (Input.GetKey(KeyCode.D))
	        {
	        	transform.position += new Vector3(Time.deltaTime * despl * (float)Math.Sin( (transform.rotation.eulerAngles.y+90) * Math.PI / 180),
	        		0.0f, Time.deltaTime * despl * (float)Math.Cos( -(transform.rotation.eulerAngles.y+90) * Math.PI / 180) );
	        	pressKey = true;
	        	counter = counter+1;
	        	//GetComponent<Camera> ().fieldOfView--;
	        }
	    
        //rotar la camara segun el piso
        if (Input.GetMouseButton(1))
        {
        	rotationX += Input.GetAxis("Mouse X") * rotate * Time.deltaTime;
        	rotationY += Input.GetAxis("Mouse Y") * rotate * Time.deltaTime;
        	rotationY = Mathf.Clamp(rotationY, minY, maxY);
        	transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }

        //sonido de caminar
        
       if(pressKey & counter==1)
       {
       		mi_caminar = Instantiate(sonido_caminar);
       }

       if(!pressKey)
       {
       		Destroy(mi_caminar);
       		counter = 0;
       }
       
    }
}
