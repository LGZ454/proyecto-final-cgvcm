using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class select_interact : MonoBehaviour
{
	public GameObject datos;
	public GameObject texto_titulo;
	public GameObject texto_informacion;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
        	if (Physics.Raycast(ray, out hit))
        	{
        		if(hit.transform.tag == "informacion")
        		{
        			Debug.Log("Mensaje 1: "+hit.transform.name);
        			CargarTexto(hit.transform.gameObject);
        			texto_informacion.SetActive(true);
        			datos.SetActive(true);
        		}
        	}
        }
    }

    void CargarTexto(GameObject interact)
    {
    	string titulo = interact.GetComponent<codigo_interactuable>().titulo;
    	string datos = interact.GetComponent<codigo_interactuable>().mi_informacion;
    	texto_titulo.GetComponent<TextMeshProUGUI>().text = titulo;
    	texto_informacion.GetComponent<TextMeshProUGUI>().text = datos;
    }
}
