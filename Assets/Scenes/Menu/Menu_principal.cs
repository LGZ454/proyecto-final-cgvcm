using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_principal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IrLugar1()
    {
    	SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }

    public void IrIglesias()
    {
    	SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+2);
    }

    public void IrMenu()
    {
    	SceneManager.LoadScene(0);
    }

	public void Cerrar_juego()
    {
    	Application.Quit();
    	Debug.Log("Salir");
    }
}
